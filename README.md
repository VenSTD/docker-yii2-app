Docker-compose config for Yii 2 Advanced Project Template
===================================

REQUIREMENTS
------------

* [Docker](https://github.com/docker/docker/releases) >= v1.10.0
* [Docker Compose](https://github.com/docker/compose/releases) >= v1.6.0

INSTALLATION
--------------------
```
git clone --depth=1 https://VenSTD@bitbucket.org/VenSTD/docker-yii2-app.git docker-yii2-app \
&& cd docker-yii2-app \
&& rm -rf .git \
&& composer create-project --prefer-dist yiisoft/yii2-app-advanced project \
&& cd project \
&& rm -rf .git \
&& cd .. \
&& sudo chown -R $USER:$USER project
```

Start docker containers
~~~
docker-compose up -d
~~~

Composer install and init Yii2
~~~
docker-compose run php composer install
docker exec run php /init
~~~

Configure: ./project/common/config/main-local.php
~~~
...
'db' => [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=<YOUR_COMPUTER_IP>;port=8306;dbname=docker_app',
    'username' => 'root',
    'password' => 'admin123',
    'charset' => 'utf8',
],
...
~~~

To migrate migrations:
1. run:
~~~
docker ps
~~~

2. Copy CONTAINER ID of IMAGE: <project_name>_php

3. SSH to that container:
~~~
docker exec -it <CONTAINER ID> /bin/bash
~~~

4. Beeing inside run:
~~~
php yii migrate
~~~

NOTE: git isn't installed in php container, so use `--prefer-dist` composer option
NOTE: default directory inside php container - "/project"

After start check http://127.0.0.1:8000


DIRECTORY STRUCTURE
-------------------
```
docker                          contains docker configurations, build files and logs
    nginx                       nginx docker configuration
    php                         php docker configuration
    mariadb                     mariadb docker configuration + dir for data
project                         Yii 2 Advanced Project Template
docker-compose.yml              docker-compose configuration
production-compose.yml          docker-compose configuration for production env
common-compose.yml              common docker-compose configuration
```


USAGE
------
To execute commands inside docker container run
~~~
docker-compose run --rm {service} {command}
or, if application already running
docker exec {service} {command}
~~~
For example:
~~~
docker-compose run php composer install
docker exec run php /init
~~~
